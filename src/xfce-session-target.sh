#!/bin/bash

if [[ "$1" == "login" ]]
then
  if ! systemctl --user is-active xfce-session.target &> /dev/null
  then
    /bin/systemctl --user import-environment DISPLAY XAUTHORITY XDG_CONFIG_DIRS XDG_CURRENT_DESKTOP XDG_DATA_DIRS XDG_GREETER_DATA_DIR XDG_MENU_PREFIX XDG_RUNTIME_DIR XDG_SEAT XDG_SEAT_PATH XDG_SESSION_CLASS XDG_SESSION_DESKTOP XDG_SESSION_ID XDG_SESSION_PATH XDG_SESSION_TYPE XDG_VTNR
    # We need all the xinitrc environment for our session, so might as well bring it all in:
    /usr/bin/dbus-update-activation-environment --systemd --all
    /bin/systemctl --user start xfce-session.target
  fi
elif [[ "$1" == "logout" ]]
then
  if systemctl --user is-active xfce-session.target &> /dev/null
  then
    /bin/systemctl --user stop xfce-session.target
  fi
fi

